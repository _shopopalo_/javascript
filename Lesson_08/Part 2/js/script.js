/*
Задание:
    Нарисовать на странице круг используя параметры, которые введет пользователь.
    При загрузке страницы - показать на ней кнопку с текстом “Нарисовать круг”. 
    Данная кнопка должна являться единственным контентом в теле HTML документа, весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript
    При нажатии на кнопку “Нарисовать круг” показывать одно поле ввода - диаметр круга. При нажатии на кнопку “Нарисовать” создать на странице 100 кругов (10*10) случайного цвета. 
    При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться, то есть все остальные круги сдвигаются влево.
*/

window.onload = () => {
    const blockBtn = document.querySelector('.btn-drawcircle');
    const blockCircles = document.querySelector('.circles');
    const btnDraw = document.getElementById('btnCircle');

    const inputCircle = document.createElement('input');
    inputCircle.classList.add('enterDiCircle');
	inputCircle.setAttribute('type', 'text');
	inputCircle.setAttribute('value', '');
	inputCircle.setAttribute('placeholder', 'Введите диаметр круга');

    const buttonDraw  = document.createElement('input');
    buttonDraw .classList.add('sBtn');
	buttonDraw .setAttribute('type', 'button');
	buttonDraw .setAttribute('value', 'Нарисовать круг');
	buttonDraw .setAttribute('id', 'buttonDraw');


    btnDraw.onclick = () => {
        btnDraw.remove();
        blockBtn.append(inputCircle);
        blockBtn.append(buttonDraw);
    }

    buttonDraw.onclick = () => {
        const diCircle = inputCircle.value + 'px';

        buttonDraw.remove();
        inputCircle.remove();

        for(let i = 0; i < 100; i++) {
            const circle = document.createElement('div');
			circle.setAttribute('class', 'circle');
			circle.style.background = `rgb(${(Math.random() * 255).toFixed(0)}, ${(Math.random() * 255).toFixed(0)}, ${(Math.random() * 255).toFixed(0)})`;
			circle.style.height = diCircle;
			circle.style.width = diCircle;
			blockCircles.append(circle);
        }
    }

    blockCircles.addEventListener('click', (event) => {
		if (event.target.classList.contains('circle')) {
			event.target.remove();
		};
	})

};
