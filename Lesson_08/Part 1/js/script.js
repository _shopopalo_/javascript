/*

Задание 1:
    Создайте меню ресторана.
    Сделайте вывод данных на страницу генерируя теги через джаваскрипт.
    Выводите название блюда, состав, вес, цену. Если позиция находится в стоп листе, то данные по позиции не выводить.

*/

const RestauranMenu = [
    {
        id: 1,
        productName: "Паста з грибами",
        productWeight: "300 г",
        ingredients: "Локшина, печериці, сир пармезан, вершки, цибуля зелена",
        price: 75,
        productImageUrl: "https://mistercat.com.ua/media/com_ksenmart/images/products/w600xh300/middle-middle-color-center-center-2020-14-07-1-0-0-100001515.png",
        keywords: ['Паста и лапша', 'Паста'],
        Weight: 300,
        stopList: false,
    },
    {
        id: 2,
        productName: "Локшина скляна з равликом",
        productWeight: "240 г",
        ingredients: "Локшина скляна, часник, цибуля, перець болгарський, морква, печериці, соус соєвий, равлики, мікс кунжуту, цибуля зелена, перець чилі, соус унагі",
        price: 129,
        productImageUrl: "https://mistercat.com.ua/media/com_ksenmart/images/products/w600xh300/middle-middle-color-center-center-2020-14-07-1-0-0-100001699.png",
        keywords: ['Паста и лапша', "Локшина"],
        Weight: 240,
        stopList: false,
    },
    {
        id: 3,
        productName: "Локшина з куркою",
        productWeight: "255 г",
        ingredients: "Локшина, часник, цибуля, перець болгарський, морква, печериці, соус соєвий, філе куряче, мікс кунжуту, цибуля зелена",
        price: 99,
        productImageUrl: "https://mistercat.com.ua/media/com_ksenmart/images/products/w600xh300/middle-middle-color-center-center-2020-14-07-1-0-0-100001520.png",
        keywords: ['Паста и лапша', 'Локшина'],
        Weight: 255,
        stopList: false,
    },
    {
        id: 4,
        productName: "Бульйон курячий",
        productWeight: "300 г",
        ingredients: "Бульйон курячий, морква, картопля, локшина, яйце куряче, філе куряче, кріп, хліб тостовий",
        price: 69,
        productImageUrl: "https://mistercat.com.ua/media/com_ksenmart/images/products/w600xh300/middle-middle-color-center-center-2020-14-07-1-0-0-100001512.png",
        keywords: ['Гарячі страви', 'Супи'],
        Weight: 300,
        stopList: false,
    },
    {
        id: 5,
        productName: "Окрошка",
        productWeight: "320 г",
        ingredients: "Яйце, картопля, кріп, цибуля, сметана, гірчиця, філе куряче, огірок, лимоний сік, хліб тостовий, редиска",
        price: 69,
        productImageUrl: "https://mistercat.com.ua/media/com_ksenmart/images/products/w600xh300/middle-middle-color-center-center-2020-14-07-1-0-0-100001524.png",
        keywords: ['Гарячі страви', 'Супи'],
        Weight: 320,
        stopList: false,
    },
    {
        id: 6,
        productName: "Суп грибний",
        productWeight: "300 г",
        ingredients: "Морква, цибуля, картопля, печериці, масло вершкове, вершки, цибуля зелена",
        price: 89,
        productImageUrl: "https://mistercat.com.ua/media/com_ksenmart/images/products/w600xh300/middle-middle-color-center-center-2020-14-07-1-0-0-100001510.png",
        keywords: ['Гарячі страви', 'Супи'],
        Weight: 300,
        stopList: false,
    },
    {
        id: 7,
        productName: "Американський мак",
        productWeight: "120 г",
        ingredients: "Мак, молоко, темний шоколад, масло, арахіс, яйце",
        price: 79,
        productImageUrl: "https://mistercat.com.ua/media/com_ksenmart/images/products/w600xh300/middle-middle-color-center-center-2020-14-07-1-0-0-35600000080.png",
        keywords: ['Десерти', 'Торти'],
        Weight: 350,
        stopList: false,
    },
    {
        id: 8,
        productName: "Тірамісу",
        productWeight: "120 г",
        ingredients: "Бисквит, сыр маскарпоне, сливки, шоколад, эспрессо, вино масала",
        price: 79,
        productImageUrl: "https://mistercat.com.ua/media/com_ksenmart/images/products/w600xh300/middle-middle-color-center-center-2020-14-07-1-0-0-35600000078.png",
        keywords: ['Десерти', 'Торти'],
        Weight: 120,
        stopList: false,
    },
    {
        id: 9,
        productName: "Солодкий рол",
        productWeight: "170 г ",
        ingredients: "Рисове тісто, крем-сир філадельфія, банан, апельсин",
        price: 109,
        productImageUrl: "https://mistercat.com.ua/media/com_ksenmart/images/products/w600xh300/middle-middle-color-center-center-2020-14-07-1-0-0-35600000083.png",
        keywords: ['Десерти', 'Роли'],
        Weight: 170,
        stopList: false,
    },
]

window.onload = () => {
    /* Создём тег main внутрь которого помещаем тег menu-container, внутри которого будут наши карточки с меню */
    const main = document.createElement('main');
    const container = document.createElement('div');
    container.classList.add('menu-container');
    document.body.append(main);
    main.append(container);

    RestauranMenu.forEach(element => {

        if (element.stopList == false) {
            
            const cards = [
                document.createElement('div'), // 0
                document.createElement('div'), // 1
                document.createElement('img'), // 2
                document.createElement('div'), // 3
                document.createElement('div'), // 4
                document.createElement('div'), // 5
                document.createElement('div'), // 6
                document.createElement('div'), // 7
                document.createElement('button') // 8
            ];

            cards[0].classList.add('card-menu');
            cards[1].classList.add('card-menu_img');
            cards[3].classList.add('card-title');
            cards[4].classList.add('card-title_name');
            cards[5].classList.add('card-title_inrgd');
            cards[6].classList.add('card-title_weight');
            cards[7].classList.add('card-menu_price');
            cards[8].classList.add('card-menu_btn');

            cards[1].append(cards[2]);
            cards[3].append(cards[4], cards[5], cards[6], cards[7], cards[8]);
            cards[0].append(cards[1], cards[3]);

            cards[2].setAttribute('src', element.productImageUrl);
            cards[4].innerText = element.productName;
            cards[5].innerText = element.ingredients;
            cards[6].innerText = element.productWeight;
            cards[7].innerText = element.price + " ₴";
            cards[8].innerText = 'Купить';

            // Добавляем карточку на страницу
            container.append(cards[0]);
        };
    });
};
