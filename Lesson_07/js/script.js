/* 
Задание 1:

    Реализовать функцию для создания объекта “пользователь”.
    Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
    При вызове функция должна спросить у вызывающего имя и фамилию.
    Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
    Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре(например, Ivan Kravchenko → ikravchenko).
    Создать пользователя с помощью функции createNewUser().Вызвать у пользователя функцию getLogin().Вывести в консоль результат выполнения функции.

*/

class createNewUser {
    constructor(fName, lName) {
        this.firstName = fName;
        this.lastName = lName;
    }

    getLogin() {
        return this.firstName + this.lastName;
    }
}

const newUser = new createNewUser(prompt('Введите ваше имя: ', ''), prompt('Введите вашу фамилию: ', ''));
console.log(newUser.getLogin());

/*

Задание 2:

    Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
    Возьмите выполненное задание выше (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
    При вызове функция должна спросить у вызывающего дату рождения(текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
    Создать метод getAge() который будет возвращать сколько пользователю лет.
    Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией(в нижнем регистре) и годом рождения (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
    Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.

*/

class createNewUser {
    constructor(fName, lName, Birthday) {
        this.firstName = fName;
        this.lastName = lName;
        this.birthday = Birthday;
    }

    getLogin() {
        return this.firstName + this.lastName;
    }

    getAge() {
        const dayNow = new Date();
        const userBirthday = Date.parse(`${this.birthday.slice(6)}-${this.birthday.slice(3, 5)}-${this.birthday.slice(0, 2)}`);
        const userAge = ((dayNow - userBirthday) / (1000 * 60 * 60 * 24 * 30 * 12)).toFixed();
        return `Ваш возраст ${userAge} лет.`;
    }

    getPaswword() {
        const userPassword = this.firstName[0].toUpperCase() + this.lastName + this.birthday.slice(6);
        return `Ваш пароль: ${userPassword}`;
        }
    }

const newUser = new createNewUser(prompt('Введите ваше имя: ', ''), prompt('Введите вашу фамилию: ', ''), prompt('Введите дату рождения (в формате DD.MM.YYYY): ', ''));
console.log(newUser.getAge());
console.log(newUser.getPaswword());

/*

Задание 3:
    Реализовать функцию фильтра массива по указанному типу данных.
    Написать функцию filterBy(), которая будет принимать в себя 2 аргумента. Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
    Функция должна вернуть новый массив, который будет содержать в себе все данные, которые были переданы в аргумент, за исключением тех, тип которых был передан вторым ргументом.
    То есть, если передать массив[‘hello’, ‘world’, 23, ‘23’, null], и вторым аргументом передать ‘string’, то функция вернет массив[23, null].

*/

function filterBy(array, dataType) {
    if(Array.isArray(array)) {
        const newArr = array.filter(el => (typeof el) === dataType);
        console.log(newArr);
    } else {
        alert(`Ошибка. Внимательнее.`);
    }
}

const array = ['Привет', 18, null, '18', undefined, function () {}, 'STRING', [], 101, '63'];
const filter = prompt('Введиет тип данных, который будет использовать для сортировки массива: ', 'undefined, null, number, string, object, function, boolean');

filterBy(array, filter);
